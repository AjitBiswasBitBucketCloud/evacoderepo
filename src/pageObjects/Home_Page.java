package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

public class Home_Page {
	private static WebElement element= null;
	
	public static WebElement lnk_myAccount(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//a[contains(text(),'My Account')]"));
		return element;
	}
	
	public static WebElement lnk_Logout(WebDriver driver)
	{
		element = driver.findElement(By.id("account_logout"));
		return element;
	}

}
