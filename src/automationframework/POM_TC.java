package automationframework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pageObjects.Home_Page;
import pageObjects.Login_Page;

public class POM_TC {
	private static WebDriver driver = null;
		public static void main(String[] args)
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\TYSS\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://www.store.demoqa.com");
		Home_Page.lnk_myAccount(driver).click();
		Login_Page.txtbx_UserName(driver).sendKeys("testuser_1");
		Login_Page.txtbx_PassWord(driver).sendKeys("Test@123");
		Login_Page.btn_LogIn(driver).click();
		System.out.println(" Login Successfully, now it is the time to Log Off buddy.");
		Home_Page.lnk_Logout(driver).click(); 
	    driver.quit();
		
	}
}
